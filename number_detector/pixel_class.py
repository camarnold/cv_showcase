from pygame.sprite import Sprite
from pygame import Surface
import pygame
from constants import *


# Define a class for our pixels/grid spaces
class Pixel(Sprite):
    def __init__(self, x, y):
        super(Pixel, self).__init__()
        self.x = TOTAL*x + 1# + SPACE_BETWEEN_PIXELS
        self.y = TOTAL*y + 1# + SPACE_BETWEEN_PIXELS
        self.state = -0.5
        self._change_color()

        self.surf = Surface((GRID_PIXEL_SIZE, GRID_PIXEL_SIZE))
        self.surf.fill(self.color)
        self.rect = self.surf.get_rect()

    def _change_color(self):
        self.color = state_dict[self.state]

    def show(self):
        screen.blit(self.surf, (self.x, self.y))

    def change_state(self, state):
        self.state = state
        self._change_color()
        self.surf.fill(self.color)
        self.rect = self.surf.get_rect()
        self.color = self.color


class Label():
    def __init__(self, txt, location, size=(7*GRID_PIXEL_SIZE+1,2*GRID_PIXEL_SIZE), fg=WHITE, bg=GRAY, font_name='OpenSans.ttf', font_size=12):
        self.bg = bg
        self.fg = fg
        self.size = size

        self.font = pygame.font.Font(font_name, font_size)
        self.txt = txt
        self.txt_surf = self.font.render(self.txt, 1, self.fg)
        self.txt_rect = self.txt_surf.get_rect(center=[s//2 for s in self.size])

        self.surface = pygame.surface.Surface(size)
        self.rect = self.surface.get_rect(topleft=location)

    def draw(self, screen):
        screen.blit(self.surface, self.rect)

    def update(self, txt):
        self.txt = txt
        self.surface.fill(self.bg)
        self.txt_surf = self.font.render(self.txt, True, self.fg)
        self.surface.blit(self.txt_surf, self.txt_rect)
