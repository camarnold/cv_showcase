#https://realpython.com/pygame-a-primer/#basic-pygame-program

from pygame import init
from make_grid import *


if __name__ == '__main__':
    # Initialize pygame
    init()

    keep_going = True
    while keep_going:
        try:
            keep_going, grid = Wall_Maker(grid)
        except:
            keep_going, grid = Wall_Maker()
