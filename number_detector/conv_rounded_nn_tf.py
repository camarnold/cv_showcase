from tensorflow import keras
import tensorflow as tf


def conv_model_maker():
    model = keras.Sequential([
            keras.layers.Conv2D(filters=32, kernel_size=(3,3), strides=(1,1), activation=keras.activations.relu, input_shape=(28,28,1)),
            keras.layers.MaxPool2D(),
            keras.layers.Conv2D(filters=64, kernel_size=(3,3), strides=(1,1), activation=keras.activations.relu),
            keras.layers.MaxPool2D(),
            keras.layers.Conv2D(filters=128, kernel_size=(3,3), strides=(1,1), activation=keras.activations.relu),

            keras.layers.Flatten(),
            keras.layers.Dropout(0.1),
            keras.layers.Dense(64, keras.activations.relu),
            keras.layers.Dropout(0.1),
            keras.layers.Dense(32, keras.activations.relu),
            keras.layers.Dense(10, keras.activations.softmax)])
    model.compile(optimizer='adam',
                  loss = keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics='accuracy')
    model.load_weights('conv_rounded_model')
    return model
