from pygame.event import get
from pygame.mouse import get_pos, get_pressed
from pygame.display import flip
from pygame.locals import K_SPACE, K_ESCAPE, KEYDOWN, QUIT, K_r, K_q
from pygame.draw import lines
from constants import *
from pixel_class import Pixel, Label
from numpy import array
from conv_rounded_nn_tf import conv_model_maker

ANIMATION_FACTOR = 100 # how slowly the bars update, 0 is instant

def Wall_Maker(grid=None):
    # what happens when we click
    def click(pos):
        x = pos[0] // (SCREEN_WIDTH // WIDTH) # find x coord
        y = pos[1] // (SCREEN_HEIGHT // HEIGHT) # find y coord
        if 0 <= x < 28 and 0 <= y < HEIGHT: # if the click was on the screen
            cur_pixel = grid[x][y]
            if cur_pixel.state == -0.5:
                cur_pixel.change_state(0.5)

    # what happens when we rightclick
    def rightclick(pos):
        x = pos[0] // (SCREEN_WIDTH // WIDTH) # find x coord
        y = pos[1] // (SCREEN_HEIGHT // HEIGHT) # find y coord
        if 0 <= x < 28 and 0 <= y < HEIGHT: # if the click was on the screen
            cur_pixel = grid[x][y]
            if cur_pixel.state == 0.5:
                cur_pixel.change_state(-0.5)

    # create input for thing
    def input_maker(grid):
        my_list = [[grid[j][i].state for j in range(28)] for i in range(28)]
        return array(my_list).reshape((1,28,28,1))

    try:
        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].change_state(0.5)
    except:
        grid = [[Pixel(i,j) for j in range(HEIGHT)] for i in range(WIDTH)]


    for i in range(28, WIDTH):
        for j in range(HEIGHT):
            grid[i][j].change_state(2)
    numbers = [Label('Hello, my name is Jeff.', (28*GRID_PIXEL_SIZE, 2.5*GRID_PIXEL_SIZE*i)) for i in range(10)]
    for i in range(10):
        numbers[i].update('{}:'.format(i))

    model = conv_model_maker()
    model_output = model(input_maker(grid))[0]
    cur_odds = [float(model_output[i]) for i in range(10)]

    running = True
    while running:

        events = get()
        for event in events:
            if get_pressed()[0]:
                try:
                    pos = get_pos()
                    click(pos)
                    model_output = model(input_maker(grid))[0]
                except AttributeError:
                    pass

            elif get_pressed()[2]:
                try:
                    pos = get_pos()
                    rightclick(pos)
                    model_output = model(input_maker(grid))[0]
                except AttributeError:
                    pass

            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return False, -1

                elif event.key == K_r:
                    return Wall_Maker()

            elif event.type == QUIT:
                return False, -1

        # draw what needs to be drawn
        screen.fill(BLACK)
        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()
        for i in range(10):
            cur_odds[i] = (ANIMATION_FACTOR*cur_odds[i] + float(model_output[i]))/(ANIMATION_FACTOR+1)
            numbers[i].draw(screen)
            lines(screen, WHITE, False, [(29*GRID_PIXEL_SIZE, (2.5*i+1)*GRID_PIXEL_SIZE), (((cur_odds[i]*(WIDTH-0.5)+(1-cur_odds[i])*29))*GRID_PIXEL_SIZE, (2.5*i+1)*GRID_PIXEL_SIZE)], width=round(3*GRID_PIXEL_SIZE/4))
        flip()
