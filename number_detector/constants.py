from pygame.display import set_mode

# Define constants
GRID_PIXEL_SIZE = 22
WIDTH = 52
HEIGHT = 28
SPACE_BETWEEN_PIXELS = 0
TOTAL = GRID_PIXEL_SIZE+SPACE_BETWEEN_PIXELS
SCREEN_WIDTH = WIDTH*TOTAL+1
SCREEN_HEIGHT = HEIGHT*TOTAL+1


# Define colors
WHITE = (255,255,255) # blank pixel
GRAY = (10,10,10) # other background
BLACK = (0,0,0) # background

# Define state dict
state_dict = {0.5:WHITE, -0.5:BLACK, 2:GRAY}

# initialize background
screen = set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
screen.fill(BLACK)
