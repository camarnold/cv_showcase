# CV_Showcase

General computer vision showcase of previous work.

The Jupyter Notebooks are pretty self-explanatory, but for the number_detector just run `python main.py` and it should work.
